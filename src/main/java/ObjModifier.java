import java.nio.FloatBuffer;



public class ObjModifier {
	static float  center_xCoordinates = 0;
	static float center_yCoordinates = 0;
	static float center_zCoordinates = 0;
	 

    public static void centerVertices(FloatBuffer buf) {
        // We often get 3D models which are not positioned at the origin of the coordinate system. Placing such models
        // on a 3D map is harder for users. One solution is to reposition such a model so that its center is at the
        // origin of the coordinate system. Your task is to implement the centering.
        //
        // FloatBuffer stores the vertices of a mesh in a continuous array of floats (see below)
        // [x0, y0, z0, x1, y1, z1, ..., xn, yn, zn]
        // This kind of layout is common in low-level 3D graphics APIs.
        // TODO: Implement your solution here

    	
    	FindCentralPoint(buf);
    	CenterAllVectors(buf);
    	
    	
    		
    		
    	
    	
    
    
    	
    		
    }
public static Float LimitFloatToSixDecimalPoints( Float UnformatedCoordinate ) {
	 String str = String.format("%.6f", UnformatedCoordinate);
	 str = str.replace(',','.');
	 return Float.parseFloat(str.toString());
}

public static void CenterAllVectors(FloatBuffer buf) {
	 float newX; 
	 float newY;
	 float newZ;
	 int n = buf.remaining();
	 int y = 1; //index for Y coordinates 
	 int z = 2; //index for Z coordinates 
	 for(int x = 0; z <= n-1 ; x +=3) { //x is the index for X coordinates 
   		
		 	//apply the centroid matrice to move each point toward the origin of coordinates 
       	newX= buf.get(x) - center_xCoordinates; 
       	newY= buf.get(y) - center_yCoordinates;
       	newZ= buf.get(z) - center_zCoordinates ;
       	
       	
       	try {
       		// limit coordinates to 6 decimal points to match input coordinates
       		newX = LimitFloatToSixDecimalPoints(newX);
       		newY = LimitFloatToSixDecimalPoints(newY);
           	newZ = LimitFloatToSixDecimalPoints(newZ);
           	// apply new coordinates to the buffer
           	buf.put( x , newX );
	        	buf.put( y , newY );
	        	buf.put( z , newZ );
       		
       	}catch (Exception e) {
       		e.printStackTrace();
       	}
       	//updates indexes that are not updates within the loop   
       	y +=3;
       	z +=3;
    
    }
	 
}

public static void FindCentralPoint(FloatBuffer buf) {
	//Calculating the coordinates of the centroid using the average method
	int n = buf.remaining();
	int y = 1;
	int z = 2;
	
	for(int x = 0; z < n ; x +=3) {
		// calculate sum of each coordinate 
		center_xCoordinates += buf.get(x);
		center_yCoordinates += buf.get(y);
		center_zCoordinates += buf.get(z);
		y +=3;
		z +=3;
	}
		center_xCoordinates /=(n/3); //devide by n/3 which represents the number of x coordinates provided
		center_yCoordinates /=(n/3); //devide by n/3 which represents the number of y coordinates provided
		center_zCoordinates /=(n/3); //devide by n/3 which represents the number of z coordinates provided
}


}
